package com.itau.funcionarios.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repositories.LocalizacaoRepository;

@Service
public class LocalizacaoService {

	@Autowired
	LocalizacaoRepository lRepository;

	public Iterable<Localizacao> buscarLocalizacoes() {
		return lRepository.findAll();
	}

}
