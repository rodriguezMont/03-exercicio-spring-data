package com.itau.funcionarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.service.LocalizacaoService;

@RestController
@RequestMapping("/localizacao")
public class LocalizacaoController {

	@Autowired
	LocalizacaoService lService;

	@GetMapping
	public Iterable<Localizacao> getLocalizacoes() {
		return lService.buscarLocalizacoes();
	}

}
